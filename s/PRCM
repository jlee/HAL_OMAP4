; Copyright 2011 Castle Technology Ltd
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
;     http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.
;

        GET     Hdr:ListOpts
        GET     Hdr:Macros
        GET     Hdr:System
        GET     Hdr:Machine.<Machine>
        GET     Hdr:ImageSize.<ImageSize>
        GET     Hdr:Proc

        GET     Hdr:OSEntries

        GET     hdr.omap4430
        GET     hdr.PRCM
        GET     hdr.StaticWS
        GET     hdr.Timers

        AREA    |Asm$$Code|, CODE, READONLY, PIC

        IMPORT  Timer_Init

        EXPORT  PRCM_SetClocks

PRCM_SetClocks
        Push    "v1-v3,lr"
        ; We calculate the system clock speed by measuring it against the 32KHz counter
        ; Use HAL's 1st timer, just so we don't have to worry about turning it off later
        ; (since it's used as HAL timer 0)
        BL      Timer_Init              ; Just (re)initialise all of them for simplicity
        LDR     a3, Timers_Log
        MOV     a1, #0
        STR     a1, [a3, #GPT_TLDR]     ; Start at 0
        MOV     a1, #GPT_TCLR_ST
        STR     a1, [a3, #GPT_TCLR]     ; Start timer
        LDR     a4, L4_32KTIMER_Log
        LDR     a1, [a4, #REG_32KSYNCNT_CR]
        ; Wait 20 ticks. But unlike u-boot, we'll properly take into account the chance of
        ; the timer overflowing
10
        LDR     a2, [a4, #REG_32KSYNCNT_CR]
        SUB     a2, a2, a1
        CMP     a2, #20
        BLT     %BT10
        ; Now begin
        LDR     a1, [a4, #REG_32KSYNCNT_CR]
        LDR     v1, [a3, #GPT_TCRR]
10
        LDR     a2, [a4, #REG_32KSYNCNT_CR]
        LDR     v2, [a3, #GPT_TCRR]
        SUB     a2, a2, a1
        CMP     a2, #20
        BLT     %BT10
        SUB     a1, v2, v1
        ; Now search the lookup table for the right entry
        ADR     a2, SysClkTable
10
        LDMIA   a2!,{a3,a4,v1,v2,v3}
        CMP     a1, a3
        BLE     %BT10
        STR     a4, sys_clk
        STR     v3, Timer_DelayMul
        Pull    "v1-v3,pc"

SysClkTable
        ;       Counter Clock speed     SYS_CLKSEL      Divider DelayMul
        DCD     19000,  38400000,       7,              2,      384
        DCD     15200,  26000000,       5,              2,      260
        DCD     11000,  19200000,       4,              1,      192
        DCD     9000,   16800000,       3,              1,      168
        DCD     7000,   12000000,       1,              1,      120
        DCD     -1,     00000000,       0,              1,      0

        END
